#! /usr/bin/env python3

"""
* A custom ``user agent`` header. You may need to set this if a podcast
  provider blocks the default `urllib` user agent.
* Remember to wrap ``root_dir`` with `~os.path.expanduser` if you want
  to use ``~`` in the root path.
* Use the ``template`` option if you want to change where the episodes
  are stored, or the file name of the audio file.
* The ``podcast`` option is just a `dict`. You can retrieve or manipulate
  this `dict` with functions that return a `dict`-like object

"""

import os
from collections import OrderedDict

import getpodcast

opt = getpodcast.options(
    date_from="2021-01-01",
    user_agent="Mozilla/5.0",
    root_dir=os.path.expanduser("~/podcast"),
    template="{rootdir}/{podcast}/{year}/{date} - {title}{ext}",
)

podcasts = {
    "SGU": "https://feed.theskepticsguide.org/feed/sgu",
    "Taskmaster": "https://feed.podbean.com/taskmasterpodcast/feed.xml",
    "Bad Voltage": "http://www.badvoltage.org/feed/mp3/",
    "Radiolab": "http://feeds.wnyc.org/radiolab",
    "Serial": "http://feeds.serialpodcast.org/serialpodcast?format=xml",
    "Neebscast": "https://www.blubrry.com/feeds/neebscast.xml",
}

podcasts = OrderedDict(sorted(podcasts.items(), key=lambda t: t[0]))

getpodcast.getpodcast(podcasts, opt)
