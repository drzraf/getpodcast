import types
from unittest.mock import Mock, patch

from pytest import raises

from getpodcast import hooks, parse_hooks_string


def test_parse_hooks_string_return_types():
    gen = parse_hooks_string("")
    assert isinstance(gen, types.GeneratorType)

    gen = parse_hooks_string("a=b")
    assert isinstance(gen, types.GeneratorType)


def test_parse_hooks_string_return_none():
    n = 0
    for res in enumerate(parse_hooks_string("")):
        n += 1
    assert n == 0


def test_parse_hooks_string_return_single_hook():
    n = 0
    for a, b in parse_hooks_string("a=b"):
        n += 1
        assert a == "a"
        assert b == "b"
    assert n == 1


def test_parse_hooks_string_return_multiple_hooks():
    n = 0
    res = (("a", "b"), ("c", "d"), ("e", "f"))
    for k, v in parse_hooks_string("a=b,c=d,e=f"):
        assert k == res[n][0]
        assert v == res[n][1]
        n += 1
    assert n == 3


def test_parse_hooks_string_exception():
    with raises(ValueError):
        list(parse_hooks_string(","))

    with raises(ValueError):
        list(parse_hooks_string("c=,"))


@patch("sys.modules")
def test_hooks_call_module_function(modules):
    module = Mock()
    module.my_hook_func = Mock()
    modules.get.return_value = module
    hooks("test_hook=my_hook_func", "test_hook", {"k": "v"})
    modules.get.assert_called_once_with("__main__", "getpodcast")
    module.my_hook_func.assert_called_once_with(k="v")


@patch("sys.modules")
def test_hooks_no_call_to_module(modules):
    module = Mock()
    module.my_hook_func = Mock()
    modules.get.return_value = module
    hooks("test_hook=my_hook_func", "other_hook", {})
    modules.get.assert_not_called()
